package com.taskactivity.TaskActivity.Service;

import com.taskactivity.TaskActivity.Entity.Task;
import com.taskactivity.TaskActivity.Repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    @Autowired
    private TaskRepository repository;

    public Task saveTask(Task task){
        return repository.save(task);
    }
    public List<Task> saveTasks(List<Task> tasks){
        return repository.saveAll(tasks);
    }

    public List<Task> getTasks(){
        return repository.findAll();
    }

    public Task getTaskById(int id){
        return repository.findById(id).orElse(null);
    }

    public String deleteTask(int id){
        repository.deleteById(id);
        return "Task removed "+ id;
    }
    public Task updateTask(Task task){
        Task existingTask=repository.findById(task.getId()).orElse(null);
        existingTask.setTitle(task.getTitle());
        existingTask.setDescription(task.getDescription());
        existingTask.setCompleted(task.isCompleted());
        return repository.save(existingTask);
    }
}
