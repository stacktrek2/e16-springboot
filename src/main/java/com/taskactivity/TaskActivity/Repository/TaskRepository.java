package com.taskactivity.TaskActivity.Repository;

import com.taskactivity.TaskActivity.Entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Integer> {
}
