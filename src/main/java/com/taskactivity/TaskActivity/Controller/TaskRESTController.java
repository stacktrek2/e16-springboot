package com.taskactivity.TaskActivity.Controller;


import com.taskactivity.TaskActivity.Entity.Task;
import com.taskactivity.TaskActivity.Service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TaskRESTController {

    @Autowired
    private TaskService service;

    @PostMapping("/addTask")
    public Task addTask(@RequestBody Task task){
        return service.saveTask(task);
    }
    @PostMapping("/addTasks")
    public List<Task> addTasks(@RequestBody List<Task> tasks){
        return service.saveTasks(tasks);
    }
    @GetMapping("/tasks")
    public List<Task> findAllTasks(){
        return service.getTasks();
    }

    @GetMapping("/task/{id}")
    public Task findTaskById(@PathVariable int id){
        return service.getTaskById(id);
    }

    @PutMapping("/update")
    public Task updateTask(@RequestBody Task task){
        return service.updateTask(task);
    }
    @DeleteMapping("/delete/{id}")
    public String deleteTask(@PathVariable int id){
        return service.deleteTask(id);
    }

}
