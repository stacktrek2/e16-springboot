package com.taskactivity.TaskActivity.Controller;


import com.taskactivity.TaskActivity.Entity.Task;
import com.taskactivity.TaskActivity.Service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskService service;

    @GetMapping("")
    public String showPage(Task task){
        return "index";
    }
    @GetMapping("/addTaskPage")
    public String showTaskForm(Task task){
        return "addnewtask";
    }
    @PostMapping("/addTask")
    public String addTask(@Validated Task task, BindingResult result, Model model){
        if(task.getTitle().isBlank() || task.getDescription().isBlank()){
            return "addnewtask";
        } else {
            service.saveTask(task);
            return "redirect:/tasks";
        }

    }
    @PostMapping("/addTasks")
    public List<Task> addTasks(@RequestBody List<Task> tasks){
        return service.saveTasks(tasks);
    }

    @GetMapping("/tasks")
    public String showTaskList(Model model){
        List<Task> listTasks = service.getTasks();
        model.addAttribute("listTasks", listTasks);
        return "tasks";
    }
    @GetMapping("/task/{id}")
    public Task findTaskById(@PathVariable int id){
        return service.getTaskById(id);
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model){
        Task task = service.getTaskById(id);
        model.addAttribute("task", task);
        return "updatetask";
    }

    @PostMapping("/update/{id}")
    public String updateTask(@PathVariable("id") int id, @Validated Task task, BindingResult result, Model model){
        if(task.getTitle().isBlank() || task.getDescription().isBlank()){
            return "updatetask";
        } else {
            service.updateTask(task);
            return "redirect:/tasks";
        }
    }

    @GetMapping("/delete/{id}")
    public String deleteTask(@PathVariable int id){
        service.deleteTask(id);
        return "redirect:/tasks";
    }

}
